# DIOPTAS JCPDS

## Description
This repository contains .jcpds files for use in the 2D integration software package DIOPTAS:
https://github.com/Dioptas/Dioptas

Details on the optimisation of the parameters given in the files can be found in:
https://doi.org/10.1080/08957959.2023.2187294

The files are split into two groups: 
1. [298 K] JCPDS files for modelling the behaviour along the 298 K isotherm only.
2. [Thermal EoS] Fully thermal EoS models representing the material behaviours across various pressures and temperatures.
